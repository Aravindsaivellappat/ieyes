/**
 * View Models used by Spring MVC REST controllers.
 */
package com.titans.barclays.web.rest.vm;
