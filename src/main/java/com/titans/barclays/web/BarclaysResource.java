package com.titans.barclays.web;

import com.hazelcast.spi.impl.operationservice.impl.responses.Response;
import com.titans.barclays.service.BarclaysAtmService;
import com.titans.barclays.service.BarclaysBanksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * Created by aravind.v on 19/07/17.
 */
@Controller
@RestController
@RequestMapping("/api")
public class BarclaysResource {

    @Autowired
    BarclaysAtmService barclaysAtmService;

    @Autowired
    BarclaysBanksService barclaysBanksService;

    public BarclaysResource(BarclaysAtmService barclaysAtmService, BarclaysBanksService barclaysBanksService) {
        this.barclaysAtmService = barclaysAtmService;
        this.barclaysBanksService = barclaysBanksService;
    }

    @GetMapping("/atms")
    public ResponseEntity<String> getAtms() {
        return barclaysAtmService.getAtmInfo();
    }

    @GetMapping("/banks")
    public ResponseEntity<String> getBanks() {
        return barclaysBanksService.getBanksInfo();
    }
}
