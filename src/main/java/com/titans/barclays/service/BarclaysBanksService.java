package com.titans.barclays.service;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.titans.barclays.service.util.Constants;
import groovy.util.logging.Slf4j;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * Created by aravind.v on 19/07/17.
 */
@Service
@Slf4j
@Data
public class BarclaysBanksService {

    private final Logger log = LoggerFactory.getLogger(BarclaysBanksService.class);

    @Value("${barclays-end-point}")
    private String barclays_end_point;

    public ResponseEntity<String> getBanksInfo() {
        ResponseEntity<String> response = null;
        try{
            StringBuilder bankUrlBuilder = new StringBuilder("https://");
            bankUrlBuilder.append(barclays_end_point);
            bankUrlBuilder.append(Constants.BARCLAYS_ATM_URL);
            String bankUrl = bankUrlBuilder.toString();
            log.info("GET barclays banks:: url --> "+ bankUrl);

            RestTemplate restTemplate = new RestTemplate();
            response = restTemplate.getForEntity(bankUrl, String.class);
            log.info("GET barclays banks:: response --> "+ response.getBody());

        } catch (Exception e) {
            log.error("Error in getting barclays banks" + e.getLocalizedMessage());
        }
        return response;
    }
}
