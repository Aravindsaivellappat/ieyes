package com.titans.barclays.service;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.mail.iap.Response;
import com.titans.barclays.service.util.Constants;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Path;

/**
 * Created by aravind.v on 19/07/17.
 */
@Service
@Slf4j
@Data
public class BarclaysAtmService implements Serializable{

    private final Logger log = LoggerFactory.getLogger(BarclaysAtmService.class);

    @Value("${barclays-end-point}")
    private String barclays_end_point;

    public ResponseEntity<String> getAtmInfo() {
        ResponseEntity<String> atms = null;
        try{
            StringBuilder atmUrlBuilder = new StringBuilder("https://");
            atmUrlBuilder.append(barclays_end_point);
            atmUrlBuilder.append(Constants.BARCLAYS_ATM_URL);
            String atmUrl = atmUrlBuilder.toString();
            log.info("Barclays atm endpoint ->" + atmUrl);

            RestTemplate restTemplate = new RestTemplate();
            atms = restTemplate.getForEntity(atmUrl, String.class);
            log.info("GET:: Barclays atms:: response -->"+ atms.getBody());

        } catch (Exception e) {
            log.error("Error getting barclays atms" + e.getLocalizedMessage());
        }
        return atms;
    }

}
