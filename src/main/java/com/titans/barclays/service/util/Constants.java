package com.titans.barclays.service.util;

/**
 * Created by aravind.v on 19/07/17.
 */
public class Constants {

    public static final String BARCLAYS_ATM_URL = "/atms";
    public static final String BARCLAYS_BRANCHES_URL = "/branches";
}
